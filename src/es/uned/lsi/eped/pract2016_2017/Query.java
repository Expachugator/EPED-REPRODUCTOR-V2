package es.uned.lsi.eped.pract2016_2017;

public class Query implements QueryIF {

	String title, author, genre, album;
	int min_year, max_year, min_duration, max_duration;

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getAuthor() {
		return author;
	}

	@Override
	public String getGenre() {
		return genre;
	}

	@Override
	public String getAlbum() {
		return album;
	}

	@Override
	public int getMin_year() {
		return min_year;
	}

	@Override
	public int getMax_year() {
		return max_year;
	}

	@Override
	public int getMin_duration() {
		return min_duration;
	}

	@Override
	public int getMax_duration() {
		return max_duration;
	}

	public Query(String title, String author, String genre, String album, int min_year, int max_year, int min_duration,
			int max_duration) {
		this.title = title != null ? title : "";
		this.author = author != null ? author : "";
		this.genre = genre != null ? genre : "";
		this.album = album != null ? album: "";
		this.min_year = min_year != -1 ? min_year : -1;
		this.max_year = max_year != -1 ? max_year : -1;
		this.min_duration = min_duration != -1 ? min_duration : -1;
		this.max_duration = max_duration != -1 ? max_duration : -1;
	}
}
