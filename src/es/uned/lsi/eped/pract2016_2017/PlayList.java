package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class PlayList implements PlayListIF {

	private ListIF<Integer> gestorLista;
	private String id;

	public PlayList() {
		gestorLista = new List<Integer>();
	}
	
	public PlayList(String id){
		gestorLista = new List<Integer>();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ListIF<Integer> getPlayList() {
		return gestorLista;
	}

	public void addListOfTunes(ListIF<Integer> lT) {

		// dada una lista de identificadores de canciones, aada dichos
		// identificadores a la lista de reproduccin

		IteratorIF<Integer> it = lT.iterator();
		while (it.hasNext()) {
			Integer guardar = it.getNext();
			Integer posicion = this.getPlayList().size() + 1;
			gestorLista.insert(guardar, posicion);
		}
	}

	@Override
	public void removeTune(int tuneID) {
		// dado un identificador de cancin, elimine toda aparicin de dicho
		// identificador en la lista de reproduccin

		IteratorIF<Integer> it = gestorLista.iterator();
		int pos = 1;
		while (it.hasNext()) {
			if (tuneID == it.getNext()) {
				gestorLista.remove(pos);
				pos--;
			}
			pos++;
		}

		// Ahora lo hago con una lista auxiliar //
		/*
		 * List<Integer> aux = new List<Integer>();
		 * 
		 * IteratorIF<Integer> it2 = gestorLista.iterator(); while
		 * (it2.hasNext()) { if (tuneID != it2.getNext()) { aux.insert(tuneID,
		 * 1); } }
		 */
		// gestorLista = aux; // Pero aqui la lista saldra al reves asi que
		// itero y guardo //

		// gestorLista.clear();

		// IteratorIF<Integer> iter = aux.iterator();
		// while (iter.hasNext()) {
		// System.out.println(tuneID);
		// gestorLista.insert(tuneID, 1);
		// }
	}
}
