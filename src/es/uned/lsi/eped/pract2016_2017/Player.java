package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class Player implements PlayerIF {

	private TuneCollection tuneCollection;
	private PlayListManager playListManager;
	private PlayBackQueue playBackQueue;
	private RecentlyPlayed recentlyPlayed;

	public Player(TuneCollection tCollection, int maxRecentlyPlayed) {

		tuneCollection = tCollection;
		playListManager = new PlayListManager();
		playBackQueue = new PlayBackQueue();
		recentlyPlayed = new RecentlyPlayed(maxRecentlyPlayed);
	}

	public void addListOfTunesToPlayBackQueue(ListIF<Integer> L) {

		/*
		 * AÃ±ade una lista de identificadores de canciones del repositorio a la
		 */
		/* cola de reproducciÃ³n */
		/* @param -una lista de identificadores de canciones contenidas en el */
		/*
		 * repositorio /* @pre -todos los elementos de la lista son
		 * identificadores de
		 */
		/* canciones que existen dentro del repositorio */
		/*
		 * @pos se aÃ±aden a la cola de reproducciÃ³n los identificadores de las
		 */
		/* canciones contenidos en la lista */

		playBackQueue.addTunes(L);
	}

	public void addListOfTunesToPlayList(String playListID, ListIF<Integer> L) { // HECHO
		/*
		 * AÃ±ade una lista de identificadores de canciones del repositorio a
		 * una
		 */
		/* lista de reproducciÃ³n */
		/*
		 * @param -una cadena de caracteres no vacÃ­a con el identificador de la
		 */
		/* lista de reproducciÃ³n a la que se van a aÃ±adir las canciones */
		/* -una lista de identificadores de canciones contenidas en el */
		/* repositorio */
		/* @pre -todos los elementos de la lista son identificadores de */
		/* canciones que existen dentro del repositorio */
		/*
		 * @pos -si existe una lista de reproducciÃ³n con ese identificador, se
		 */
		/* aÃ±aden a ella los identificadores contenidos en la lista */
		/* -en caso contrario, no se hace nada */

		playListManager.getPlayList(playListID).addListOfTunes(L);

	}

	public void addPlayListToPlayBackQueue(String playListID) {
		/* AÃ±ade el contenido de una lista de reproducciÃ³n a la cola de */
		/* reproducciÃ³n */
		/*
		 * @param -una cadena de caracteres no vacÃ­a con el identificador de la
		 */
		/* lista de reproducciÃ³n cuyo contenido se desea aÃ±adir a la cola */
		/* de reproducciÃ³n */
		/*
		 * @pos -si existe una lista de reproducciÃ³n con se identificador, se
		 */
		/* aÃ±ade su contenido a la cola de reproducciÃ³n */
		/* -en caso contrario, no se hace nada */

		PlayListIF playList = playListManager.getPlayList(playListID);

		if (playList != null) {
			ListIF<Integer> lista = playList.getPlayList();
			playBackQueue.addTunes(lista);
		}
	}

	public void addSearchToPlayBackQueue(String title, String author, String genre, String album, int min_year,
			int max_year, int min_duration, int max_duration) {

		/*
		 * AÃ±ade los identificadores de todas las canciones del repositorio que
		 */
		/* cumplan los criterios indicados a la cola de reproducciÃ³n */
		/*
		 * @param -una cadena de caracteres con el tÃ­tulo de la canciÃ³n
		 * buscada
		 */
		/* -una cadena de caracteres con el autor de la canciÃ³n buscada */
		/* -una cadena de caracteres con el gÃ©nero de la canciÃ³n buscada */
		/* -una cadena de caracteres con el Ã¡lbum al que pertenece la */
		/* canciÃ³n buscada */
		/* -un entero con el primer aÃ±o del intervalo en el que se creÃ³ */
		/* la canciÃ³n a buscar */
		/* -un entero con el Ãºltimo aÃ±o del intervalo en el que se creÃ³ */
		/* la canciÃ³n a buscar */
		/* -un entero con la duraciÃ³n mÃ­nima de la canciÃ³n a buscar */
		/* -un entero con la duraciÃ³n mÃ¡xima de la canciÃ³n a buscar */
		/* @pos se aÃ±aden a la cola de reproducciÃ³n los identificadores de */
		/* todas las canciones del repositorio que cumplan todos los */
		/* criterios indicados */

		ListIF<Integer> list = new List<Integer>();

		Query query = new Query(title, author, genre, album, min_year, max_year, min_duration, max_duration);

		int pos = 1;
		for (int i = 0; i < tuneCollection.size(); i++) {
			Tune tune = tuneCollection.getTune(i);
			if (tune.match(query)) {
				list.insert(i, pos);
				pos++;
			}
		}
		playBackQueue.addTunes(list);
	}

	public void addSearchToPlayList(String playListID, String title, String author, String genre, String album,
			int min_year, int max_year, int min_duration, int max_duration) {

		/*
		 * AÃ±ade los identificadores de todas las canciones del repositorio que
		 */
		/* cumplan los criterios indicados a una lista de reproducciÃ³n */
		/*
		 * @param -una cadena de caracteres no vacÃ­a con el identificador de la
		 */
		/* lista de reproducciÃ³n a la que se van a aÃ±adir las canciones */
		/* -una cadena de caracteres con el tÃ­tulo de la canciÃ³n buscada */
		/* -una cadena de caracteres con el autor de la canciÃ³n buscada */
		/* -una cadena de caracteres con el gÃ©nero de la canciÃ³n buscada */
		/* -una cadena de caracteres con el Ã¡lbum al que pertenece la */
		/* canciÃ³n buscada */
		/* -un entero con el primer aÃ±o del intervalo en el que se */
		/* publicÃ³ la canciÃ³n a buscar */
		/* -un entero con el Ãºltimo aÃ±o del intervalo en el que se */
		/* publicÃ³ la canciÃ³n a buscar */
		/* -un entero con la duraciÃ³n mÃ­nima de la canciÃ³n a buscar */
		/* -un entero con la duraciÃ³n mÃ¡xima de la canciÃ³n a buscar */
		/*
		 * @pos -si existe una lista de reproducciÃ³n con se identificador, se
		 */
		/* aÃ±aden a ella los identificadores de todas las canciones del */
		/* repositorio que cumplan todos los criterios indicados */
		/* -en caso contrario, no se hace nada */

		ListIF<Integer> list = new List<Integer>();
		Query query = new Query(title, author, genre, album, min_year, max_year, min_duration, max_duration);
		PlayListIF playList;

		if (playListManager.contains(playListID)) {
			playList = playListManager.getPlayList(playListID);
			int pos = 1;
			for (int i = 0; i < tuneCollection.size(); i++) {
				Tune tune = tuneCollection.getTune(i);
				if (tune.match(query) == true) {
					list.insert(i, pos);
					pos++;
				}
			}
			playList.addListOfTunes(list);
		}
	}

	public void clearPlayBackQueue() {

		/* VacÃ­a la cola de reproducciÃ³n */
		/* @pos -la cola de reproducciÃ³n se vacÃ­a */

		playBackQueue.clear();

	}

	public void createPlayList(String playListID) { // HECHO
		/* Crea una nueva lista de reproducciÃ³n a partir de su identificador */
		/*
		 * @param -una cadena de caracteres no vacÃ­a con el identificador de la
		 */
		/* nueva lista de reproducciÃ³n */
		/*
		 * @pos -si no existe una lista de reproducciÃ³n con ese identificador,
		 */
		/* se crea */
		/* -en caso contrario, no se hace nada */

		playListManager.createPlayList(playListID);

	}

	public void play() {

		/* Reproduce la siguiente canciÃ³n en la cola de reproducciÃ³n */
		/*
		 * @pos -si la cola de reproducciÃ³n no es vacÃ­a, se elimina de ella el
		 */
		/* primer elemento, pasando Ã©ste a la estructura que almacena las */
		/* Ãºltimas canciones reproducidas, sin sobrepasar su tamaÃ±o */
		/* mÃ¡ximo */
		/* -en caso contrario, no se hace nada */

		if (playBackQueue.isEmpty() == false) {
			recentlyPlayed.addTune(playBackQueue.getFirstTune());
			playBackQueue.extractFirstTune();
		}
	}

	public void removePlayList(String playListID) { // HECHO
		/* Elimina una lista de reproducciÃ³n del reproductor a partir de su */
		/* identificador */
		/*
		 * @param -una cadena de caracteres no vacÃ­a con el identificador de la
		 */
		/* lista de reproducciÃ³n a eliminar */
		/*
		 * @pos -si existe una lista de reproducciÃ³n con ese identificador, se
		 */
		/* elimina */
		/* -en caso contrario, no se hace nada */

		playListManager.removePlayList(playListID);
	}

	public void removeTuneFromPlayList(String playListID, int tuneID) {

		/* Elimina una canciÃ³n de una lista de reproducciÃ³n */
		/*
		 * @param -una cadena de caracteres no vacÃ­a con el identificador de la
		 */
		/* lista de reproducciÃ³n de la que se quiere eliminar la canciÃ³n */
		/* -un entero con el identificador de la canciÃ³n del repositorio */
		/* que se quiere eliminar de dicha lista */
		/*
		 * @pos -si existe una lista de reproducciÃ³n con se identificador, se
		 */
		/* elimina de dicha lista todas las apariciones del identificador */
		/* de la canciÃ³n del repositorio pasada como parÃ¡metro */
		/* -en caso contrario, no se hace nada */

		playListManager.getPlayList(playListID).removeTune(tuneID);
	}

	public ListIF<Integer> getPlayBackQueue() { // HECHO
		/*
		 * Devuelve los identificadores de las canciones contenidas en la cola
		 * de
		 */
		/* reproducciÃ³n */
		/*
		 * @return una lista con los identificadores de las canciones que estÃ¡n
		 */
		/* en la cola de reproducciÃ³n (ha de conservar el orden en el que */
		/* se introdujeron las canciones) */

		return playBackQueue.getContent();
	}

	public ListIF<Integer> getPlayListContent(String playListID) { // HECHO
		/* Devuelve el contenido de una lista de reproducciÃ³n */
		/*
		 * @param -una cadena de caracteres no vacÃ­a con el identificador de la
		 */
		/* lista de reproducciÃ³n de la que se quiere obtener su contenido */
		/*
		 * @return -si en el reproductor existe una lista de reproducciÃ³n con
		 * ese
		 */
		/* identificador, se devolverÃ¡ una lista con su contenido */
		/* -en caso contrario, se devolverÃ¡ una lista vacÃ­a */

		return playListManager.getPlayList(playListID).getPlayList();
	}

	public ListIF<String> getPlayListIDs() { // HECHO
		/* Devuelve los identificadores de todas las listas de reproducciÃ³n */
		/* existentes */
		/* @returns -una lista con los identificadores de todas las listas de */
		/* reproducciÃ³n (no importa el orden) */

		return playListManager.getIDs();
	}

	public ListIF<Integer> getRecentlyPlayed() { // HECHO
		/*
		 * Devuelve los identificadores de las Ãºltimas canciones reproducidas
		 * que
		 */
		/* estÃ¡n almacenadas en RecentlyPlayed */
		/*
		 * @return una lista con los identificadores de las Ãºltimas canciones
		 */
		/* reproducidas (en el orden inverso al que se reprodujeron) */

		return recentlyPlayed.getContent();
	}

}