package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

public class RecentlyPlayed implements RecentlyPlayedIF {

	private ListIF<Integer> gestorPilaRecientes;

	int max;

	public RecentlyPlayed(int max) {
		this.max = max;
		gestorPilaRecientes = new List<Integer>();
	}

	@Override
	public ListIF<Integer> getContent() {
		/*
		 * Devuelve los identificadores de las últimas canciones reproducidas
		 * en
		 */
		/* el orden inverso al que fueron reproducidas */
		/*
		 * @return una lista con los identificadores de las últimas canciones
		 */
		/* reproducidas (en el orden inverso al que se reprodujeron) */

		return gestorPilaRecientes;
	}

	@Override
	public void addTune(int tuneID) {
		/* Añade la última canción reproducida */
		/* @param -un entero con el identificador de la última canción */
		/* reproducida */
		/* @pos -se añade el identificador a la estructura que almacena las */
		/* últimas canciones reproducidas, garantizándose que no se */
		/* almacenan más canciones que las marcadas por el valor máximo */
		/* permitido indicado en el constructor */

		if (gestorPilaRecientes.size() == max) {
			gestorPilaRecientes.remove(max);
		}
		gestorPilaRecientes.insert(tuneID, 1);
	}
}
