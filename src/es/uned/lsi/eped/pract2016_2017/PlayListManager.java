package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.Tree;
import es.uned.lsi.eped.DataStructures.TreeIF;

public class PlayListManager implements PlayListManagerIF {

	private TreeIF<Object> gestorListaReproduccion;

	public PlayListManager() {
		gestorListaReproduccion = new Tree<Object>();
		gestorListaReproduccion.setRoot('@');
	}



	
	@Override
	public boolean contains(String playListID) {

		/*
		 * Comprueba si existe una lista de reproducción dado su identificador
		 */
		/*
		 * @param -una cadena de caracteres no vacía con el identificador de la
		 */
		/* lista de reproducción que queremos saber si existe o no */
		/* @return -un valor booleano indicando si existe o no una lista de */
		/* reproducción asociada al identificador recibido como parámetro */

		boolean existe = false;

		existe = gestorListaReproduccion.contains(playListID);

		return existe;
	}

	@Override
	public PlayListIF getPlayList(String playListID) {
		/* Devuelve la lista de reproducci�n asociada a un identificador */
		/*
		 * @param -una cadena de caracteres no vac�a con el identificador de la
		 */
		/* lista de reproducci�n que queremos recuperar */
		/* @pre -existe una lista de reproducci�n asociada al identificador */
		/* que se recibe como par�metro */
		/*
		 * @return -la lista de reproducci�n asociada al identificador recibido
		 */
		/* como par�metro */

		return aux(gestorListaReproduccion, 0, playListID);
	}

	private PlayListIF aux(TreeIF<Object> arbol, int pos, String cadena) {

		PlayListIF play = new PlayList();
		ListIF<TreeIF<Object>> auxiliar = arbol.getChildren();
		if (auxiliar.isEmpty()) {
			return play;
		}
		if (pos >= cadena.length()) {
			IteratorIF<TreeIF<Object>> it2 = auxiliar.iterator();
			while (it2.hasNext()) {
				TreeIF<Object> aux = it2.getNext();
				Object raiz = aux.getRoot();
				if (raiz instanceof PlayListIF) {
					play = (PlayListIF) raiz;
					return play;
				}
			}
		} else {
			IteratorIF<TreeIF<Object>> it = auxiliar.iterator();
			while (it.hasNext()) {
				TreeIF<Object> aux = it.getNext();
				Object raiz = aux.getRoot();
				if ((raiz instanceof Character) && (raiz.equals(cadena.valueOf(pos)))) {
					// if ((raiz instanceof Character) &&
					// (raiz.equals(cadena[pos])) {
					play = aux(aux, pos++, cadena);
				}
			}
		}

		// if ((root instanceof Character) && (root.equals(play[pos]))) {

		return play;

	}

	@Override
	public ListIF<String> getIDs() {

		/* Devuelve una lista con todos los identificadores de las listas de */
		/* reproducción existentes */
		/*
		 * @return -una lista de cadenas de caracteres (todas no vacías) que
		 * son
		 */
		/* los identificadores de todas las listas de reproducción */
		/* existentes */

		ListIF<String> listaID = new List<String>();

		ListIF<TreeIF<Object>> listaHijos = gestorListaReproduccion.getChildren();
		
		IteratorIF<TreeIF<Object>> it = listaHijos.iterator();
		
		while (it.hasNext()){
			TreeIF<Object> hijo = it.getNext();
			
			if(hijo instanceof PlayList){
				listaID.insert(hijo.toString(), 1);
			}
		}
		
				
		/*
			ListIF<TreeIF<Object>> list = arbol.getChildren();
			IteratorIF<TreeIF<Object>> it = list.iterator();
			while (it.hasNext()) {
				TreeIF<Object> subarbol = it.getNext();
				Object root = subarbol.getRoot();
				if ((root instanceof Character) && (root.equals(play[pos]))) {
					auxiliar(subarbol, playList, pos++);
				}
			}
		*/	
	
		return listaID;
	}

	@Override
	public void createPlayList(String playListID) {

		/*
		 * Crea una nueva lista de reproducción vacía y la asocia a un nuevo
		 */
		/* identificador */
		/*
		 * @param -una cadena de caracteres no vacía con el identificador de la
		 */
		/* lista de reproducción que queremos crear */
		/* @pre -no existe ninguna lista de reproducción asociada al */
		/* identificador recibido como parámetro */

		// if (gestorListaReproduccion.isLeaf()) { // Si es hoja hay que crear
		// el
		// // nodo
		//
		//
		//
		// } else { // si no es hoja hay que mirar si esta el nodo de nuestra
		// letra
		//
		// for (int i = 0; i < playListID.length(); i++) {
		// boolean encontrado = buscar(playListID.valueOf(i));
		// if (encontrado) { // Si lo encuentra pasamos al siguiente
		// // caracter
		//
		// } else { // Si no lo encuentra creamos el nodo
		// crear(playListID.valueOf(i));
		// }
		// }
		// }
		//
		// if (!this.contains(playListID))
		//
		// {
		//
		// PlayList list = new PlayList();
		// list.setId(playListID);
		//
		// if (gestorListaReproduccion.getChildren().isEmpty()) {
		// gestorListaReproduccion.addChild(1, new Tree<Object>());
		// gestorListaReproduccion.getChild(1).setRoot(playListID.valueOf(1));
		// }
		//
		// playListID.length();
		// int pos = 0;
		// playListID.valueOf(pos);
		//
		// }

		auxiliar(gestorListaReproduccion, playListID, 0);
	}

	private void auxiliar(TreeIF<Object> arbol, String playList, int pos) {

		// mirar de hacer algo para no estar haciendo esto a cada llamada de
		// auxiliar
		char[] play = playList.toCharArray();

		if (pos == playList.length()) {

			crearPlaylist(arbol, playList);

		} else if (arbol.getChildren().isEmpty()) {

			TreeIF<Object> aux = crearLetra(play[pos], arbol);

			auxiliar(aux, playList, pos + 1);

		} else {

			ListIF<TreeIF<Object>> list = arbol.getChildren();

			IteratorIF<TreeIF<Object>> it = list.iterator();

			while (it.hasNext()) {

				TreeIF<Object> subarbol = it.getNext();

				Object root = subarbol.getRoot();

				if ((root instanceof Character) && (root.equals(play[pos]))) {

					auxiliar(subarbol, playList, pos++);
				}
			}
		}

	}

	private boolean buscar(String letra) {

		boolean encontrado;
		encontrado = gestorListaReproduccion.contains(letra);
		return encontrado;
	}

	private TreeIF<Object> crearLetra(char letra, TreeIF<Object> arbol) {

		arbol.addChild(1, (new Tree<Object>(letra)));

		return arbol.getChild(1);
	}

	private void crearPlaylist(TreeIF<Object> arbol, String playlistID) {

		PlayList playlist = new PlayList(playlistID);

		arbol.addChild(1, new Tree<Object>(playlist));
	}

	@Override
	public void removePlayList(String playListID) {

		/* Elimina una lista de reproducción asociada a un identificador */
		/*
		 * @param -una cadena de caracteres no vacía con el identificador de la
		 */
		/* lista de reproducción que queremos eliminar */
		/* @pre -existe una lista de reproducción asociada al identificador */
		/* recibido como parámetro */

	}

}

/*
 * 
 * private ListIF<PlayListIF> gestorListaReproduccion;
 * 
 * public PlayListManager() { gestorListaReproduccion = new List<PlayListIF>();
 * }
 * 
 * @Override public boolean contains(String playListID) { /* Comprueba si existe
 * una lista de reproducción dado su identificador
 */
/*
 * @param -una cadena de caracteres no vacía con el identificador de la
 */
/* lista de reproducción que queremos saber si existe o no */
/* @return -un valor booleano indicando si existe o no una lista de */
/* reproducción asociada al identificador recibido como parámetro */

/*
 * IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator();
 * 
 * while (it.hasNext()) { PlayListIF aux = it.getNext(); if (((PlayList)
 * aux).getId().equals(playListID)) { return true; } } return false; }
 * 
 * @Override public PlayListIF getPlayList(String playListID) { /* Devuelve la
 * lista de reproducción asociada a un identificador
 */
/*
 * @param -una cadena de caracteres no vacía con el identificador de la
 */
/* lista de reproducción que queremos recuperar */
/* @pre -existe una lista de reproducción asociada al identificador */
/* que se recibe como parámetro */
/*
 * @return -la lista de reproducción asociada al identificador recibido
 */
/* como parámetro */

/*
 * PlayListIF play = new PlayList(); IteratorIF<PlayListIF> it =
 * gestorListaReproduccion.iterator();
 * 
 * while (it.hasNext()) { PlayListIF aux = it.getNext(); if (((PlayList)
 * aux).getId().equals(playListID)) { return aux; } } return play; }
 * 
 * @Override public ListIF<String> getIDs() {
 * 
 * /* Devuelve una lista con todos los identificadores de las listas de
 */
/* reproducción existentes */
/*
 * @return -una lista de cadenas de caracteres (todas no vacías) que son
 */
/* los identificadores de todas las listas de reproducción */
/* existentes */

/*
 * ListIF<String> list = new List<String>(); IteratorIF<PlayListIF> it =
 * gestorListaReproduccion.iterator(); while (it.hasNext()) { PlayListIF aux =
 * it.getNext(); list.insert(((PlayList) aux).getId(), 1); } return list; }
 * 
 * @Override public void createPlayList(String playListID) { /* Crea una nueva
 * lista de reproducción vacía y la asocia a un nuevo
 */
/* identificador */
/*
 * @param -una cadena de caracteres no vacía con el identificador de la
 */
/* lista de reproducción que queremos crear */
/* @pre -no existe ninguna lista de reproducción asociada al */
/* identificador recibido como parámetro */

/*
 * if (this.contains(playListID) == false){ PlayList list = new PlayList();
 * list.setId(playListID); gestorListaReproduccion.insert(list, 1); } }
 * 
 * @Override public void removePlayList(String playListID) {
 * 
 * int pos = 1;
 * 
 * IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator(); while
 * (it.hasNext()) { PlayListIF aux = it.getNext(); if ((((PlayList)
 * aux).getId()).equals(playListID)) { gestorListaReproduccion.remove(pos); }
 * pos++; } } }
 * 
 */

//////////////////////////////////

/*
 * 
 * private ListIF<PlayListIF> gestorListaReproduccion;
 * 
 * public PlayListManager() { gestorListaReproduccion = new List<PlayListIF>();
 * }
 * 
 * @Override public boolean contains(String playListID) { /* Comprueba si existe
 * una lista de reproducción dado su identificador
 */
/*
 * @param -una cadena de caracteres no vacía con el identificador de la
 */
/* lista de reproducción que queremos saber si existe o no */
/* @return -un valor booleano indicando si existe o no una lista de */
/* reproducción asociada al identificador recibido como parámetro */

/*
 * IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator();
 * 
 * while (it.hasNext()) { PlayListIF aux = it.getNext(); if (((PlayList)
 * aux).getId().equals(playListID)) { return true; } } return false; }
 * 
 * @Override public PlayListIF getPlayList(String playListID) { /* Devuelve la
 * lista de reproducción asociada a un identificador
 */
/*
 * @param -una cadena de caracteres no vacía con el identificador de la
 */
/* lista de reproducción que queremos recuperar */
/* @pre -existe una lista de reproducción asociada al identificador */
/* que se recibe como parámetro */
/*
 * @return -la lista de reproducción asociada al identificador recibido
 */
/* como parámetro */

/*
 * PlayListIF play = new PlayList(); IteratorIF<PlayListIF> it =
 * gestorListaReproduccion.iterator();
 * 
 * while (it.hasNext()) { PlayListIF aux = it.getNext(); if (((PlayList)
 * aux).getId().equals(playListID)) { return aux; } } return play; }
 * 
 * @Override public ListIF<String> getIDs() {
 * 
 * /* Devuelve una lista con todos los identificadores de las listas de
 */
/* reproducción existentes */
/*
 * @return -una lista de cadenas de caracteres (todas no vacías) que son
 */
/* los identificadores de todas las listas de reproducción */
/* existentes */

/*
 * ListIF<String> list = new List<String>(); IteratorIF<PlayListIF> it =
 * gestorListaReproduccion.iterator(); while (it.hasNext()) { PlayListIF aux =
 * it.getNext(); list.insert(((PlayList) aux).getId(), 1); } return list; }
 * 
 * @Override public void createPlayList(String playListID) { /* Crea una nueva
 * lista de reproducción vacía y la asocia a un nuevo
 */
/* identificador */
/*
 * @param -una cadena de caracteres no vacía con el identificador de la
 */
/* lista de reproducción que queremos crear */
/* @pre -no existe ninguna lista de reproducción asociada al */
/* identificador recibido como parámetro */

/*
 * if (this.contains(playListID) == false){ PlayList list = new PlayList();
 * list.setId(playListID); gestorListaReproduccion.insert(list, 1); } }
 * 
 * @Override public void removePlayList(String playListID) {
 * 
 * int pos = 1;
 * 
 * IteratorIF<PlayListIF> it = gestorListaReproduccion.iterator(); while
 * (it.hasNext()) { PlayListIF aux = it.getNext(); if ((((PlayList)
 * aux).getId()).equals(playListID)) { gestorListaReproduccion.remove(pos); }
 * pos++; } } }
 * 
 */