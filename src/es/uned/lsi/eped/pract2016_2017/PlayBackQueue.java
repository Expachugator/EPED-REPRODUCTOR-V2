package es.uned.lsi.eped.pract2016_2017;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;
import es.uned.lsi.eped.DataStructures.Queue;
import es.uned.lsi.eped.DataStructures.QueueIF;

public class PlayBackQueue implements PlayBackQueueIF {

	private QueueIF<Integer> gestorCola;

	public PlayBackQueue() {
		gestorCola = new Queue<Integer>();
	}

	@Override
	public ListIF<Integer> getContent() {
		
		/* Devuelve una lista con los identificadores de las canciones contenidas
			en la cola de reproduccion
			@return -una lista de enteros con los identificadores de las canciones
		 * que estÃ¡n en la cola de reproducciÃ³n, conservando el orden en
		 */
		/* el que fueron originalmente introducidos */

		ListIF<Integer> listaId = new List<Integer>();
		IteratorIF<Integer> it = gestorCola.iterator();

		int pos = 1;
		while (it.hasNext()) {
			listaId.insert(it.getNext(), pos);
			pos++;
		}

		return listaId;
	}

	@Override
	public boolean isEmpty() { // Hecho
		/*
		 * Devuelve un booleano indicando si la cola de reproducciÃ³n es
		 * vacÃ­a o no
		 */
		/* @return -devuelve un valor booleano que indica si la cola de */
		/* reproducciÃ³n estÃ¡ vacÃ­a o no */

		return gestorCola.isEmpty();
	}

	@Override
	public int getFirstTune() { // Hecho
		/*
		 * Devuelve un entero con el identificador de la primera canciÃ³n que
		 * estÃ en la cola de reproducciÃ³n
		 * 
		 * /* @pre -la cola de reproducciÃ³n no estÃ¡ vacÃ­a
		 */
		/*
		 * @return -devuelve el identificador de la primera canciÃ³n en la
		 * cola de
		 */
		/* reproducciÃ³n */

		return gestorCola.getFirst();
	}

	@Override
	public void extractFirstTune() { // Hecho
		/*
		 * Extrae la primera canciÃ³n que se encuentre en la cola de
		 * reproducciÃ³n
		 */
		/* @pre -la cola de reproducciÃ³n no estÃ¡ vacÃ­a */
		/*
		 * @pos -elimina de la cola de reproducciÃ³n el primer identificador
		 */

		gestorCola.dequeue();
	}

	@Override
	public void addTunes(ListIF<Integer> lT) { // Hecho, falta mirar que se
												// añadan al final
		/* AÃ±ade una lista de identificadores de canciones a la cola de */
		/* reproducciÃ³n */
		/*
		 * @param -una lista de enteros con los identificadores de las canciones
		 */
		/* que se desea aÃ±adir a la lista de reproducciÃ³n */
		/* @pre -todos los elementos de la lista son identificadores de */
		/* canciones que existen dentro del repositorio */
		/* @pos -aÃ±ade todos los identificadores presentes en la lista al */
		/* final de la cola de reproducciÃ³n */

		IteratorIF<Integer> it = lT.iterator();
		while (it.hasNext()) {
			gestorCola.enqueue(it.getNext());
		}
	}

	@Override
	public void clear() { // Hecho
		/* VacÃ­a el contenido de la cola de reproducciÃ³n */
		/*
		 * @pos -la cola de reproducciÃ³n queda vacÃ­a, sin identificadores
		 */

		gestorCola.clear();
	}
}
