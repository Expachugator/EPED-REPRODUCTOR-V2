package es.uned.lsi.eped.pract2016_2017;

public class Tune implements TuneIF {

	private String title, author, genre, album;
	private int year, duration;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Tune(String title, String author, String genre, String album, int year, int duration) {
		this.title = title != null ? title : "";
		this.author = author != null ? author : "";
		this.genre = genre != null ? genre : "";
		this.album = album != null ? album: "";
		this.year = year;
		this.duration = duration;
	}

	@Override
	public boolean match(QueryIF q) {
		
		  /* Dado un objeto QueryIF conteniendo unos criterios de bÃºsqueda, devuelve */
		  /* un valor de verdad indicando si la canciÃ³n los cumple o no los cumple   */
		  /* @param   -un objeto QueryIF con unos criterios de bÃºsqueda              */
		  /* @return  -si la canciÃ³n cumple TODOS los criterios, devolverÃ¡ verdadero */
		  /*          -si la canciÃ³n incumple algÃºn criterio, devolverÃ¡ falso        */

		return (q.getTitle().equals("") || this.title.equalsIgnoreCase(q.getTitle())) &&
			(q.getAuthor().equals("") || this.author.equalsIgnoreCase(q.getAuthor())) && 
			(q.getGenre().equals("") || this.genre.equalsIgnoreCase(q.getGenre())) &&
			(q.getAlbum().equals("") || this.album.equalsIgnoreCase(q.getAlbum())) &&
			(q.getMin_year() == -1 || this.year >= q.getMin_year()) &&
			(q.getMax_year() == -1 || this.year <= q.getMax_year()) &&
			(q.getMin_duration() == -1 || this.duration >= q.getMin_duration()) && 
			(q.getMax_duration() == -1 || this.duration <= q.getMax_duration());
	}
}
